package main

import (
	"fmt"
	"time"
)

var company string = "telkom"

func main() {
	start := time.Now()
	s := make([]string, 1000)
	for i := range s {
		s[i] = company
	}

	for a := range s {
		if s[a] == "telkom" {
			fmt.Println("Company Found!")
		}
	}
	t := time.Now()
	performance := t.Sub(start)
	fmt.Println(performance)
}
