package main

import (
	"fmt"
	"time"
)

var foods = [4]string{"Sate", "Bakso", "Dimsum", "Rames"}
var drinks = [4]string{"Jeruk", "Teh", "Kelapa", "Cendol"}

func main() {
	var counter int = 1
	start := time.Now()
	for i := range foods {
		for j := range drinks {
			fmt.Println("Menu ", counter, " ", foods[i], " dan ", drinks[j])
			counter++
		}
	}
	t := time.Now()
	performance := t.Sub(start)
	fmt.Println(performance)
	//fmt.Println(len(s))
}
