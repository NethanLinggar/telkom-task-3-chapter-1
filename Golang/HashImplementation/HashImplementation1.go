package main

import (
	"fmt"
)

type HashTable struct {
	data [][]string
}

func (h *HashTable) _hash(key string) int {
	hash := 0
	for i := 0; i < len(key); i++ {
		hash = (hash + int(key[i])*i) % len(h.data)
	}
	return hash
}

func (h *HashTable) Set(key string, value string) [][]string {
	address := h._hash(key)
	if h.data[address] == nil {
		h.data[address] = make([]string, 0)
	}
	h.data[address] = append(h.data[address], key, value)
	return h.data
}

func (h *HashTable) Get(key string) string {
	address := h._hash(key)
	currentBucket := h.data[address]
	if currentBucket != nil {
		for i := 0; i < len(currentBucket); i += 2 {
			if currentBucket[i] == key {
				return currentBucket[i+1]
			}
		}
	}
	return ""
}

func main() {
	myHashTable := HashTable{make([][]string, 100)}
	myHashTable.Set("082124606606", "Rony Setyawan")
	fmt.Println(myHashTable.Get("082124606606"))

}
