package main

import "fmt"

func arrange(array []string, memory []string) [][]string {
	var results [][]string
	if memory == nil {
		memory = []string{}
	}
	for i := 0; i < len(array); i++ {
		current := array[i]
		newArray := append([]string{}, array[:i]...)
		newArray = append(newArray, array[i+1:]...)
		if len(newArray) == 0 {
			results = append(results, append(memory, current))
		}
		results = append(results, arrange(newArray, append(memory, current))...)
	}
	return results
}

func main() {
	var counter int = 0
	candidates := []string{"Baswedan", "Subianto", "Maharani", "Ganjar"}
	chairs := arrange(candidates, nil)
	for i := 0; i < len(chairs); i++ {
		fmt.Println(chairs[i])
		counter++
	}
	fmt.Println(counter)
}
