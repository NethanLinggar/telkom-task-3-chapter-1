package main

import (
	"fmt"
	"strconv"
)

func generateData() []string {
	const number string = "0821234567"
	var customer []string
	var mobileNumber string

	for i := 0; i < 100; i++ {
		if i < 10 {
			mobileNumber = number + "0" + strconv.Itoa(i)
		} else {
			mobileNumber = number + strconv.Itoa(i)
		}
		customer = append(customer, mobileNumber)
	}
	return customer
}

func sendPromoDiscount(array []string) {
	for i := 0; i < len(array); i++ {
		fmt.Println("Sending Promo to ", array[i])
	}
	for i := 0; i < 10; i++ {
		fmt.Println("Sending Discount to Chosen Customer ", i+1)
	}
}

func main() {
	var customers = generateData()
	sendPromoDiscount(customers)
}
