package main

import (
	"fmt"
	"strconv"
)

func PadLeft(str string, length int) string {
	for len(str) < length {
		str = "0" + str
	}
	return str
}

func generateData(n int) []string {
	const number string = "082"
	var customer []string
	var mobileNumber string

	for i := 0; i < n; i++ {
		mobileNumber = number + PadLeft(strconv.Itoa(i), 9)
		customer = append(customer, mobileNumber)
	}
	return customer
}

func sendPromoDiscount(array []string, array2 []string) {
	for i := 0; i < len(array); i++ {
		fmt.Println("Sending Promo to ", array[i])
	}
	fmt.Println("Its Finished to send Promo to ", len(array), " Customers")
	for i := 0; i < len(array2); i++ {
		fmt.Println("Sending Discount to ", array2[i])
	}
	fmt.Println("Its Finished to send Discount to ", len(array2), " Customers")
}

func main() {
	var dataPromo = generateData(1000000000)
	var dataDiscount = generateData(1000)
	sendPromoDiscount(dataPromo, dataDiscount)
}
