package main

import (
	"fmt"
	"math/rand"
)

var telco = [11]string{"Telkom", "Indosat", "XL", "Verizon", "AT&T", "Nippon", "Vodafone", "Orange", "KDDI", "Telefonica", "T-Mobile"}

func main() {
	var company int = rand.Intn(10)
	for i := 0; i < len(telco); i++ {
		if telco[company] == telco[i] {
			fmt.Println("Company Found: ", telco[company])
			break
		}
		fmt.Println("Searching Company...", i+1)

	}
	fmt.Println()
}
