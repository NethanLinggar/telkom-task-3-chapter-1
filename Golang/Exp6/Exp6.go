package main

import (
	"fmt"
	"time"
)

var address string = "DKI Jakarta"

func main() {
	start := time.Now()
	s := make([]string, 100000)
	for i := range s {
		s[i] = address
	}
	fmt.Println("The Default Address is " + s[0])
	t := time.Now()
	performance := t.Sub(start)
	fmt.Println(performance)
	// fmt.Println(len(s))
}
