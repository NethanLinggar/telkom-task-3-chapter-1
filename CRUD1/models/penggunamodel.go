package models

import (
	"database/sql"
	"fmt"
	"time"

	"crudMongoGolang/config"
	"crudMongoGolang/entities"
)

type PenggunaModel struct {
	conn *sql.DB
}

func NewPenggunaModel() *PenggunaModel {
	conn, err := config.DBConnection()
	if err != nil {
		panic(err)
	}

	return &PenggunaModel{
		conn: conn,
	}
}

func (p *PenggunaModel) FindAll() ([]entities.Pengguna, error) {

	rows, err := p.conn.Query("select * from pengguna")
	if err != nil {
		return []entities.Pengguna{}, err
	}
	defer rows.Close()

	var dataPengguna []entities.Pengguna
	for rows.Next() {
		var pengguna entities.Pengguna
		rows.Scan(&pengguna.Id,
			&pengguna.NamaLengkap,
			&pengguna.NIK,
			&pengguna.JenisKelamin,
			&pengguna.TempatLahir,
			&pengguna.TanggalLahir,
			&pengguna.Alamat,
			&pengguna.NoHp)

		if pengguna.JenisKelamin == "1" {
			pengguna.JenisKelamin = "Laki-laki"
		} else {
			pengguna.JenisKelamin = "Perempuan"
		}
		// 2006-01-02 => yyyy-mm-dd
		tgl_lahir, _ := time.Parse("2006-01-02", pengguna.TanggalLahir)
		// 02-01-2006 => dd-mm-yyyy
		pengguna.TanggalLahir = tgl_lahir.Format("02-01-2006")

		dataPengguna = append(dataPengguna, pengguna)
	}

	return dataPengguna, nil

}

func (p *PenggunaModel) Create(pengguna entities.Pengguna) bool {

	result, err := p.conn.Exec("insert into pengguna (nama_lengkap, nik, jenis_kelamin, tempat_lahir, tanggal_lahir, alamat, no_hp) values(?,?,?,?,?,?,?)",
		pengguna.NamaLengkap, pengguna.NIK, pengguna.JenisKelamin, pengguna.TempatLahir, pengguna.TanggalLahir, pengguna.Alamat, pengguna.NoHp)

	if err != nil {
		fmt.Println(err)
		return false
	}

	lastInsertId, _ := result.LastInsertId()

	return lastInsertId > 0
}

func (p *PenggunaModel) Find(id int64, pengguna *entities.Pengguna) error {

	return p.conn.QueryRow("select * from pengguna where id = ?", id).Scan(
		&pengguna.Id,
		&pengguna.NamaLengkap,
		&pengguna.NIK,
		&pengguna.JenisKelamin,
		&pengguna.TempatLahir,
		&pengguna.TanggalLahir,
		&pengguna.Alamat,
		&pengguna.NoHp)
}

func (p *PenggunaModel) Update(pengguna entities.Pengguna) error {

	_, err := p.conn.Exec(
		"update pengguna set nama_lengkap = ?, nik = ?, jenis_kelamin = ?, tempat_lahir = ?, tanggal_lahir = ?, alamat = ?, no_hp = ? where id = ?",
		pengguna.NamaLengkap, pengguna.NIK, pengguna.JenisKelamin, pengguna.TempatLahir, pengguna.TanggalLahir, pengguna.Alamat, pengguna.NoHp, pengguna.Id)

	if err != nil {
		return err
	}

	return nil
}

func (p *PenggunaModel) Delete(id int64) {
	p.conn.Exec("delete from pengguna where id = ?", id)
}
