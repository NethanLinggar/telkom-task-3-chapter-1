package penggunacontroller

import (
	"html/template"
	"net/http"
	"strconv"

	"crudMongoGolang/libraries"

	"crudMongoGolang/models"

	"crudMongoGolang/entities"
)

var validation = libraries.NewValidation()
var penggunaModel = models.NewPenggunaModel()

func Index(response http.ResponseWriter, request *http.Request) {

	pengguna, _ := penggunaModel.FindAll()

	data := map[string]interface{}{
		"pengguna": pengguna,
	}

	temp, err := template.ParseFiles("views/pengguna/index.html")
	if err != nil {
		panic(err)
	}
	temp.Execute(response, data)
}

func Add(response http.ResponseWriter, request *http.Request) {

	if request.Method == http.MethodGet {
		temp, err := template.ParseFiles("views/pengguna/add.html")
		if err != nil {
			panic(err)
		}
		temp.Execute(response, nil)
	} else if request.Method == http.MethodPost {

		request.ParseForm()

		var pengguna entities.Pengguna
		pengguna.NamaLengkap = request.Form.Get("nama_lengkap")
		pengguna.NIK = request.Form.Get("nik")
		pengguna.JenisKelamin = request.Form.Get("jenis_kelamin")
		pengguna.TempatLahir = request.Form.Get("tempat_lahir")
		pengguna.TanggalLahir = request.Form.Get("tanggal_lahir")
		pengguna.Alamat = request.Form.Get("alamat")
		pengguna.NoHp = request.Form.Get("no_hp")

		var data = make(map[string]interface{})

		vErrors := validation.Struct(pengguna)

		if vErrors != nil {
			data["pengguna"] = pengguna
			data["validation"] = vErrors
		} else {
			data["pesan"] = "Data pengguna berhasil disimpan"
			penggunaModel.Create(pengguna)
		}

		temp, _ := template.ParseFiles("views/pengguna/add.html")
		temp.Execute(response, data)
	}

}

func Edit(response http.ResponseWriter, request *http.Request) {

	if request.Method == http.MethodGet {

		queryString := request.URL.Query()
		id, _ := strconv.ParseInt(queryString.Get("id"), 10, 64)

		var pengguna entities.Pengguna
		penggunaModel.Find(id, &pengguna)

		data := map[string]interface{}{
			"pengguna": pengguna,
		}

		temp, err := template.ParseFiles("views/pengguna/edit.html")
		if err != nil {
			panic(err)
		}
		temp.Execute(response, data)

	} else if request.Method == http.MethodPost {

		request.ParseForm()

		var pengguna entities.Pengguna
		pengguna.Id, _ = strconv.ParseInt(request.Form.Get("id"), 10, 64)
		pengguna.NamaLengkap = request.Form.Get("nama_lengkap")
		pengguna.NIK = request.Form.Get("nik")
		pengguna.JenisKelamin = request.Form.Get("jenis_kelamin")
		pengguna.TempatLahir = request.Form.Get("tempat_lahir")
		pengguna.TanggalLahir = request.Form.Get("tanggal_lahir")
		pengguna.Alamat = request.Form.Get("alamat")
		pengguna.NoHp = request.Form.Get("no_hp")

		var data = make(map[string]interface{})

		vErrors := validation.Struct(pengguna)

		if vErrors != nil {
			data["pengguna"] = pengguna
			data["validation"] = vErrors
		} else {
			data["pesan"] = "Data pengguna berhasil diperbarui"
			penggunaModel.Update(pengguna)
		}

		temp, _ := template.ParseFiles("views/pengguna/edit.html")
		temp.Execute(response, data)
	}

}

func Delete(response http.ResponseWriter, request *http.Request) {

	queryString := request.URL.Query()
	id, _ := strconv.ParseInt(queryString.Get("id"), 10, 64)

	penggunaModel.Delete(id)

	http.Redirect(response, request, "/pengguna", http.StatusSeeOther)
}
