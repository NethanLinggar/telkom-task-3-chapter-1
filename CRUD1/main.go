package main

import (
	"net/http"

	"crudMongoGolang/controllers/penggunacontroller"
)

func main() {

	http.HandleFunc("/", penggunacontroller.Index)
	http.HandleFunc("/pengguna", penggunacontroller.Index)
	http.HandleFunc("/pengguna/index", penggunacontroller.Index)
	http.HandleFunc("/pengguna/add", penggunacontroller.Add)
	http.HandleFunc("/pengguna/edit", penggunacontroller.Edit)
	http.HandleFunc("/pengguna/delete", penggunacontroller.Delete)

	http.ListenAndServe(":3000", nil)
}
