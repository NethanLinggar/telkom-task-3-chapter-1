#include <iostream>
#include <vector>
#include <string>

using namespace std;

vector<string> generateData(int n) {
    const string baseNumber = "082";
    vector<string> customers;
    for (int i = 0; i < n; i++) {
        string mobileNumber = baseNumber + to_string(i).insert(0, 9 - to_string(i).length(), '0');
        customers.push_back(mobileNumber);
    }
    return customers;
}

void sendPromoDiscount(vector<string> input1, vector<string> input2) {
    for (int i = 0; i < input1.size(); i++) {
        cout << "Sending Promo to " << input1[i] << endl;
    }
    cout << "Its Finished to Send Promo to " << input1.size() << " Customers" << endl;

    for (int i = 0; i < input2.size(); i++) {
        cout << "Sending Discount to " << input2[i] << endl;
    }
    cout << "Its Finished to Send Discount to " << input2.size() << " Customers" << endl;
}

int main() {
    vector<string> dataPromo = generateData(100000000);
    vector<string> dataDiscount = generateData(1000);
    sendPromoDiscount(dataPromo, dataDiscount);
    return 0;
}
