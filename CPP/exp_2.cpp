#include <iostream>
#include <chrono>
#include <vector>
#include <string>

using namespace std;
using namespace std::chrono;

void findCompany(const vector<string>& array) {
    auto tx = high_resolution_clock::now();
    for (int i = 0; i < array.size(); i++) {
        if (array[i] == "telkom") {
            cout << "Company Found!" << endl;
        }
    }
    auto ty = high_resolution_clock::now();
    auto duration = duration_cast<milliseconds>(ty - tx);
    cout << "The performance is " << duration.count() << " ms" << endl;
}

int main() {
    const string company = "telkom";
    vector<string> largeCompanyName(1000, company);

    findCompany(largeCompanyName);

    return 0;
}
