#include <iostream>
#include <chrono>
#include <vector>
#include <string>

using namespace std;
using namespace std::chrono;

void logPairs(string array1[], string array2[], string words) {
    int counter = 0;
    for (int i = 0; i < 4; i++){
        for (int j = 0; j < 4; j++){
            counter++;
            cout << words << " " << counter << " " << array1[i] << " dan " << array2[j] << endl;
        }
    }
}

int main() {
    string foods[4] = {"Sate", "Bakso", "Dimsum", "Rames"};
    string drinks[4] = {"Jeruk", "Teh", "Kelapa", "Cendol"};

    logPairs(foods, drinks, "Menu");

    return 0;
}
