#include <iostream>
#include <vector>
#include <string>

using namespace std;

vector<string> generateData() {
    const string number = "0821234567";
    vector<string> customers;
    string mobileNumber;
    for (int i = 0; i < 100; i++) {
        if (i < 10){
            mobileNumber = number + "0" + to_string(i);
        } else {
            mobileNumber = number + to_string(i);
        }
        customers.push_back(mobileNumber);
    }
    return customers;
}

void sendPromoDiscount(vector<string> array) {
    for (int i = 0; i < array.size(); i++) {
        cout << "Sending Promo to " << array[i] << endl;
    }
    for (int i = 0; i < array.size(); i++){
        cout << "Sending Discount to Chosen Customer " << (i+1) << endl;
    }
}

int main() {
    vector<string> customers = generateData();
    sendPromoDiscount(customers);
    return 0;
}
