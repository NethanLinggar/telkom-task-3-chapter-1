#include <iostream>
#include <string>
#include <vector>

class HashTable {
  public:
  HashTable(int size) {
    data.resize(size);
  }

  int _hash(std::string key) {
    int hash = 0;
    for (int i = 0; i < key.length(); i++) {
      hash = (hash + key[i] * i) % data.size();
    }
    return hash;
  }

  void set(std::string key, std::string value) {
    int address = _hash(key);
    if (data[address].empty()) {
      data[address] = std::vector<std::pair<std::string, std::string>>{};
    }
    data[address].push_back(std::make_pair(key, value));
  }

  std::string get(std::string key) {
    int address = _hash(key);
    auto currentBucket = data[address];
    if (!currentBucket.empty()) {
      for (auto i = 0; i < currentBucket.size(); i++) {
        if (currentBucket[i].first == key) {
          return currentBucket[i].second;
        }
      }
    }
    return "undefined";
  }

  private:
  std::vector<std::vector<std::pair<std::string, std::string>>> data;
};

int main() {
  HashTable myHashTable(100);
  myHashTable.set("082124606606", "Rony Setyawan");
  std::cout << myHashTable.get("082124606606") << std::endl;
  return 0;
}