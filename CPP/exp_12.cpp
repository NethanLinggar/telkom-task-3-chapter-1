#include <iostream>
#include <vector>
#include <string>

using namespace std;

vector<string> generateData(int n) {
    const string baseNumber = "082";
    vector<string> customers;
    for (int i = 0; i < n; i++) {
        string mobileNumber = baseNumber + to_string(i).insert(0, 9 - to_string(i).length(), '0');
        customers.push_back(mobileNumber);
    }
    return customers;
}

void sendPromoDiscount(vector<string> input) {
    for (int i = 0; i < input.size(); i++) {
        cout << "Sending Promo to " << input[i] << endl;
    }
    cout << "Its Finished to Send Promo to " << input.size() << " Customers" << endl;

    for (int i = 0; i < input.size(); i++) {
        cout << "Sending Discount to " << input[i] << endl;
    }
    cout << "Its Finished to Send Discount to " << input.size() << " Customers" << endl;
}

int main() {
    vector<string> data = generateData(1000);
    sendPromoDiscount(data);
    return 0;
}
