#include <iostream>
#include <chrono>
#include <vector>
#include <string>

using namespace std;
using namespace std::chrono;

void findAddress(const vector<string>& array) {
    auto tx = high_resolution_clock::now();
    cout << "The Default Address is " << array[0] << endl;
    auto ty = high_resolution_clock::now();
    auto duration = duration_cast<milliseconds>(ty - tx);
    cout << "The performance is " << duration.count() << " ms" << endl;
}

int main() {
    const string address = "DKI Jakarta";
    vector<string> addresses(1000, address);

    findAddress(addresses);

    return 0;
}
