#include <iostream>
#include <chrono>
#include <vector>
#include <string>
#include <ctime>

using namespace std;
using namespace std::chrono;

void findCompany(const vector<string>& array, const int input) {
  for (int i = 0; i < array.size(); i++) {
    if (array[i] == array[input]) {
      cout << "Company Found: " << array[input] << endl;
      break; 
    }
    cout << "Searching Company... " << i + 1 << endl;
  }
}

int main() {
  vector<string> telco = {"Telkom", "Indosat", "XL", "Verizon", "AT&T", "Nippon", "Vodafone", "Orange", "KDDI", "Telefonica", "T-Mobile"};
  
  srand(time(0));

  const int company = rand() % 11 + 1;
  
  findCompany(telco, company);

  return 0;
}
