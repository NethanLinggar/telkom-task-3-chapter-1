#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

vector<vector<string>> results;
vector<string> candidates = {"Baswedan", "Subianto", "Maharani", "Ganjar"};

void arrange(vector<string> array, vector<string> memory = {}) {
    string current;
    for (int i = 0; i < array.size(); i++)
    {
        current = array[i];
        array.erase(array.begin() + i);
        if (array.size() == 0)
        {
            memory.push_back(current);
            results.push_back(memory);
        }
        memory.push_back(current);
        arrange(array, memory);
        memory.pop_back();
        array.insert(array.begin() + i, current);
    }
}

int main() {
    arrange(candidates);
    for (const auto& chair : results) {
        for (const auto& candidate : chair) {
            cout << candidate << "\t";
        }
        cout << endl;
    }
    return 0;
}
